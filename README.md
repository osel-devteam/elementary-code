<h1 align="center">
  <br>
  <a href="https://github.com/elementary/code"><img src="https://gitlab.com/uploads/-/system/project/avatar/35453195/code.png" alt="Elementary Code logo" width="180"></a>
  <br>
  Elementary Code
  <br>
</h1>

<h3 align="center">Flatpak program version and instructions for manual compilation
</h3>

<p align="center">
  <a href="#download">Download</a> •
  <a href="#manual-compilation">Manual compilation</a> • 
  <a href="#license">License</a> • 
  <a href="#general-maintainers">General maintainers</a> • 
  <a href="#contact-us">Contact us</a> 
</p>

<p align="center">
  <a href="#"><img src="https://github.com/elementary/code/raw/master/data/screenshot.png?raw=true" alt="screenshot">
</p>

## Download
You can download the latest build of the program [here](https://gitlab.com/osel-devteam/elementary-code/-/releases)

## Manual compilation
1. Installing the flatpak
   ```bash
   sudo apt install flatpak (Ubuntu, Debian, Mint)
   sudo dnf install flatpak (Fedora, CentOS, RHEL)
   sudo pacman -Sy flatpak (Arch, Manjaro)
   sudo zypper install flatpak (openSUSE)
   ```
2. Add the flatpak repo
   ```bash
   flatpak remote-add --if-not-exists --system appcenter https://flatpak.elementary.io/repo.flatpakrepo
   ```
3. Install "io.elementary.Platform" and "io.elementary.Sdk":
   ```bash
   flatpak install io.elementary.Platform io.elementary.Sdk
   ```
   **or**
  
   ```bash
   flatpak install -y appcenter io.elementary.Platform io.elementary.Sdk
   ```
4. Install dependencies  
   ```bash
   sudo apt install meson flatpak-builder (Ubuntu, Debian, Mint)
   sudo dnf install meson flatpak-builder (Fedora, CentOS, RHEL)
   sudo pacman -S meson flatpak-builder (Arch, Manjaro)
   sudo zypper install meson flatpak-builder (openSUSE)
   ```
5. Build from manifest

   **For user**
      ```bash
      flatpak-builder --user --install --force-clean build-dir io.elementary.code.yml
      ```
   **For system**
      ```bash
      flatpak-builder --system --install --force-clean build-dir io.elementary.code.yml
      ```

6. Run the app
   ```bash
   flatpak run io.elementary.code
   ```

## License
Distributed under the GPL-3.0 License

## General maintainers
+ [Artem Dadashjants](https://gitlab.com/tfastgame)
+ [Ivan Neagiry](https://gitlab.com/Neagiry)

## Contact us
If you have any questions or suggestions for cooperation or improvement of this assembly, please write to us by [e-mail](mailto:ketronix2@gmail.com) or [telegram](https://t.me/crazy_linux_chat)
