Ž            )   ü            ”  #   §  %   Ė     ń  "     !   *     L     \     h     p       $        «     ³     Ą     Ģ  C   å  '   )  l   Q  '   ¾     ę     ķ     õ     ü     	               %     +  „  H     ī  #   ō  %        >  "   T  !   w          ©     µ     ½     Ģ  $   Ó     ų                  C   2  '   v  l     '   	     3	     :	     B	     I	     V	     _	     e	     r	     x	                       	                                 
                                                                                 About Additional plugins must be disabled An additional plugin must be disabled An error occurred: %s Dependency ā%sā failed to load Dependency ā%sā was not found Disable Plugins E_nable All Enabled Failed to load Plugin Plugin loader ā%sā was not found Plugins Pr_eferences Preferences Run from build directory The following plugins depend on ā%sā and will also be disabled: The plugin ā%sā could not be loaded The ā%sā plugin depends on the ā%sā plugin.
If you disable ā%sā, ā%sā will also be disabled. There was an error displaying the help. _About _Cancel _Close _Disable All _Enabled _Help _Preferences _Quit ā libpeas demo application Project-Id-Version: libpeas
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libpeas/issues
PO-Revision-Date: 2019-08-25 01:17+0100
Last-Translator: Zander Brown <zbrown@gnome.org>
Language-Team: English - United Kingdom <en_GB@li.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 3.32.1
 About Additional plugins must be disabled An additional plugin must be disabled An error occurred: %s Dependency ā%sā failed to load Dependency ā%sā was not found Disable Plugins E_nable All Enabled Failed to load Plugin Plugin loader ā%sā was not found Plugins Pr_eferences Preferences Run from build directory The following plugins depend on ā%sā and will also be disabled: The plugin ā%sā could not be loaded The ā%sā plugin depends on the ā%sā plugin.
If you disable ā%sā, ā%sā will also be disabled. There was an error displaying the help. _About _Cancel _Close _Disable All _Enabled _Help _Preferences _Quit ā libpeas demo application 