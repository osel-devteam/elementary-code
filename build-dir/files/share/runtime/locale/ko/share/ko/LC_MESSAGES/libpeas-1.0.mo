��            )   �      �     �  #   �  %   �     �  "     !   *     L     \     h     p       $   �     �     �     �     �  C   �  '   )  l   Q  '   �     �     �     �     �     	               %     +  }  H     �  7   �  7        =  ;   O  "   �     �     �     �  &   �     	  ,        C     P     b      p  J   �  6   �  �   	  :   �	  
   �	  
   �	  
   �	     �	  
   
     
     -
     ?
     M
                       	                                 
                                                                                 About Additional plugins must be disabled An additional plugin must be disabled An error occurred: %s Dependency “%s” failed to load Dependency “%s” was not found Disable Plugins E_nable All Enabled Failed to load Plugin Plugin loader “%s” was not found Plugins Pr_eferences Preferences Run from build directory The following plugins depend on “%s” and will also be disabled: The plugin “%s” could not be loaded The “%s” plugin depends on the “%s” plugin.
If you disable “%s”, “%s” will also be disabled. There was an error displaying the help. _About _Cancel _Close _Disable All _Enabled _Help _Preferences _Quit — libpeas demo application Project-Id-Version: libpeas master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libpeas/issues
PO-Revision-Date: 2019-08-26 10:40+0900
Last-Translator: Changwoo Ryu <cwryu@debian.org>
Language-Team: Korean <gnome-kr@googlegroups.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 정보 플러그인을 추가로 사용 중지해야 합니다 플러그인을 추가로 사용 중지해야 합니다 오류 발생: %s “%s” 의존성을 읽어들이는데 실패했습니다 “%s” 의존성이 없습니다 플러그인 사용 중지 모두 사용(_N) 사용 읽어들이는데 실패했습니다. 플러그인 “%s” 플러그인 로더가 없습니다 플러그인 기본 설정(_E) 기본 설정 빌드 디렉터리에서 실행 다음 플러그인이 “%s”에 의존하므로 사용 중지합니다: “%s” 플러그인을 읽어들일 수 없습니다 “%s” 플러그인이 “%s” 플러그인에 의존합니다.
“%s”을(를) 사용 중지하면 “%s”도 사용 중지합니다: 도움말을 표시하는데 오류가 발생했습니다. 정보(_A) 취소(_C) 닫기(_C) 모두 사용 중지(_D) 사용(_E) 도움말(_H) 기본 설정(_P) 끝내기(_Q) — libpeas 데모 프로그램 