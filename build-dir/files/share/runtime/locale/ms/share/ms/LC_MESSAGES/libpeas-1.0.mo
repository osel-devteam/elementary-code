��            )   �      �     �  #   �  %   �     �  "     !   *     L     \     h     p       $   �     �     �     �     �  C   �  '   )  l   Q  '   �     �     �     �     �     	               %     +  �  H     �  "   �  '   	     1  %   H     n     �     �  
   �     �     �  !   �     �  
   �  	          :   /  "   j  `   �  .   �     	     &	     -	     4	     D	     P	  
   Y	     d	     l	                       	                                 
                                                                                 About Additional plugins must be disabled An additional plugin must be disabled An error occurred: %s Dependency “%s” failed to load Dependency “%s” was not found Disable Plugins E_nable All Enabled Failed to load Plugin Plugin loader “%s” was not found Plugins Pr_eferences Preferences Run from build directory The following plugins depend on “%s” and will also be disabled: The plugin “%s” could not be loaded The “%s” plugin depends on the “%s” plugin.
If you disable “%s”, “%s” will also be disabled. There was an error displaying the help. _About _Cancel _Close _Disable All _Enabled _Help _Preferences _Quit — libpeas demo application Project-Id-Version: libpeas master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libpeas/issues
PO-Revision-Date: 2020-01-27 14:26+0800
Language-Team: Pasukan Terjemahan GNOME Malaysia
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
Last-Translator: abuyop <abuyop@gmail.com>
X-Generator: Poedit 2.0.6
 Perihal Pemalam tambahan perlu dilumpuhkan Satu pemalam tambahan perlu dilumpuhkan Satu ralat berlaku: %s Dependensi "%s" telah gagal dimuatkan Dependensi "%s" tidak ditemui Lumpuhkan Pemalam Be_narkan Semua Dibenarkan Gagal dimuatkan Pemalam Pemuat pemalam "%s" tidak ditemui Pemalam K_eutamaan Keutamaan Jalankan dari direktori binaan Pemalam berikut bergantung pada "%s" dan juga dilumpuhkan: Pemalam "%s" tidak dapat dimuatkan Pemalam "%s" bergantung pada pemalam "%s".
Jika anda lumpuhkan "%s", "%s" juga akan dilumpuhkan. Terdapat satu ralat ketika memaparkan bantuan. Perih_al _Batal T_utup _Lumpukan Semua Dib_enarkan _Bantuan _Keutamaan _Keluar — aplikasi demo libpeas 